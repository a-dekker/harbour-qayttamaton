/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef OAUTH_H
#define OAUTH_H

#include <QObject>

class OAuth : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString code READ code NOTIFY codeChanged)
    Q_PROPERTY(QString oauthClientId READ oauthClientId WRITE setOauthClientId)
    Q_PROPERTY(QString oauthRedirectUrl READ oauthRedirectUrl WRITE setOauthRedirectUrl)

public:
    QString code() const;

    QString oauthClientId() const;
    void setOauthClientId(const QString &oauthClientId);

    QString oauthRedirectUrl() const;
    void setOauthRedirectUrl(const QString &oauthRedirectUrl);

    explicit OAuth(QObject *parent = nullptr);

signals:
    void codeChanged(const QString &code);

public slots:
    void getCode();

private slots:
    void on_TcpServer_NewConnection();
    void on_TcpSocket_readyRead();

private:
    QString m_code;
    QString m_clientId;
    QString m_redirectUrl;

};

#endif // OAUTH_H
