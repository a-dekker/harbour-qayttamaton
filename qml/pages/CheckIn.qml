/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../utils/UntappdAPI.js" as Untappd

Dialog {
    id: dialog


    property var beerInfo
    property var parameters
    property string foursquare_id: ""
    property real geolat
    property real geolng
    property string time_zone: Qt.formatDateTime(new Date, 't')

    property bool fb_activity: settings.readData("facebook", "") !== ""
    property bool tw_activity: settings.readData("twitter", "") !== ""
    property bool fs_activity: settings.readData("foursquare", "") !== ""

    VenuesListModel { id: venuesList }

    Column {
        anchors.fill: parent

        spacing: Theme.paddingMedium

        DialogHeader {
            title: qsTr("Check In")
        }

        Row {
            width: parent.width - ( Theme.paddingLarge * 2 )
            height: width / 6
            spacing: Theme.paddingMedium
            x: Theme.paddingLarge
            Image {
                width: parent.width * 0.2
                height: parent.height
                source: beerInfo.beer_label
            }
            Text {
                width: parent.width * 0.7
                height: parent.height
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeLarge
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.WordWrap
                text: beerInfo.beer_name
            }
        }

        // Comment
        Column {
            width: parent.width
            spacing: Theme.paddingSmall
            x: Theme.paddingLarge
            Label {
                text: qsTr("Comment:")
            }
            TextArea {
                id: comment
                width: parent.width
                height: parent.width * 0.3
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeMedium
            }
        }

        // Rating
        Row {
            width: parent.width
            height: rating.height
            x: Theme.paddingLarge
            Label {
                id: rating_label
                width: contentWidth
                height: parent.height
                font.pixelSize: Theme.fontSizeMedium
                verticalAlignment: Text.AlignVCenter
                text: qsTr("Rating:")
            }
            Slider {
                id: rating
                width: parent.width - ( rating_label.width + rating_value.width )
                maximumValue: 5.0
                minimumValue: 0
                stepSize: 0.25
                value: beerInfo.auth_rating
            }
            Text {
                id: rating_value
                width: parent.width * 0.2
                height: parent.height
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.secondaryHighlightColor
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: rating.value
            }
        }

        // Social
        Column {
            width: parent.width
            Row {
                width: parent.width
                TextSwitch {
                    id: twitter_btn
                    width: parent.width / 2
                    checked: false
                    enabled: tw_activity
                    text: "Twitter"
                }
                TextSwitch {
                    id: facebook_btn
                    width: parent.width / 2
                    checked: false
                    enabled: fb_activity
                    text: "Facebook"
                }
            }
            Row {
                width: parent.width
                TextSwitch {
                    id: foursquare_btn
                    width: parent.width / 2
                    checked: false
                    enabled: fs_activity && foursquare_id !== ""
                    text: "Foursquare"
                }
            }
        }
        // Venue
        Row {
            width: parent.width - ( Theme.paddingLarge * 2 )
            spacing: Theme.paddingSmall
            x: Theme.paddingLarge
            Label {
                font.pixelSize: Theme.fontSizeMedium
                verticalAlignment: Text.AlignVCenter
                text: qsTr("Venue:")
            }
            Text {
                id: venueName
                font.pixelSize: Theme.fontSizeMedium
                verticalAlignment: Text.AlignVCenter
                color: Theme.secondaryColor
                text: "New Location"
                MouseArea {
                    anchors.fill: parent
                    onClicked: if (foursquare_id === "") {
                                   var venueSearch = pageStack.push(Qt.resolvedUrl("VenuesSearch.qml"))
                                   venueSearch.venueChanged.connect(function() {
                                       venueName.text = venueSearch.venue.name
                                       foursquare_id = venueSearch.venue.id
                                       foursquare_btn.enabled = fs_activity && true
                                       foursquare_btn.checked = fs_activity &&true
                                       geolat = venueSearch.venue.location.lat
                                       geolng = venueSearch.venue.location.lng
                                   })
                               } else {
                                   parent.text = "New Location"
                                   foursquare_id = ""
                                   foursquare_btn.checked = false
                                   foursquare_btn.enabled = false
                                   geolat = 0.0
                                   geolng = 0.0
                               }
                }
            }
        }
        ListView{
            id: venue_list
            width: parent.width
            height: parent.height / 8
            orientation: Qt.Horizontal

            model: venuesList

            delegate: Rectangle {
                width: venue_list.width / 3
                height: venue_list.height
                color: "#00000000"

                Rectangle {
                    width: parent.width * 0.9
                    height: parent.height * 0.9
                    color: "#00000000"
                    anchors.centerIn: parent
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 20
                    clip: true

                    Text {
                        width: parent.width * 0.8
                        height: parent.height * 0.8
                        anchors.centerIn: parent
                        verticalAlignment: Text.AlignVCenter
                        color: Theme.secondaryColor
                        wrapMode: Text.WrapAnywhere
                        font.pixelSize: Theme.fontSizeExtraSmall
                        text: venue_name
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked:  {
                            venueName.text = venue_name
                            foursquare_id = foursquare.foursquare_id
                            foursquare_btn.enabled = fs_activity && true
                            foursquare_btn.checked = fs_activity && true
                            geolat = location.lat
                            geolng = location.lng
                        }
                    }
                }
            }
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            parameters = {
                "gmt_offset": tzOffset,
                "timezone": timeZone,
                "bid": beerInfo.bid,
                "twitter": twitter_btn.checked ? "on" : "off",
                "facebook": facebook_btn.checked ? "on" : "off",
                "foursquare": foursquare_btn.checked ? "on" : "off",
                "geolat": geolat,
                "geolng": geolng,
                "foursquare_id": foursquare_id,
                "rating": rating.value,
                "shout": comment.text
            }
        }
    }

}
