/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../utils/UntappdAPI.js" as Untappd

Dialog {
    id: dialog

    property string request
    property string user_id
    property string targetId

    DialogHeader {
        id: header
    }

    Text {
        id: message
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        color: Theme.primaryColor
        font.pixelSize: Theme.fontSizeLarge
    }

    Component.onCompleted: switch (request) {
                           case "add":
                               header.title = qsTr("Add Friend");
                               message.text = qsTr("Realy want to be a friend ?");
                               break;
                           case "remove":
                               header.title = qsTr("Remove Friend");
                               message.text = qsTr("Realy want to remove from friends ?");
                               break;
                            case "accept":
                               header.title = qsTr("Accept the request");
                               message.text = qsTr("Realy want to accept the request ?");
                               break;
                           case "reject":
                               header.title = qsTr("Reject the request");
                               message.text = qsTr("Realy want to Reject the request ?");
                               break;
                           default:
                           }

    onDone: {
        if (result == DialogResult.Accepted) {
            targetId = user_id;
        }
    }
}
