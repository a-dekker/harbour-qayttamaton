/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../components"

Page {
    id: page

    property var action_type : { "toast": qsTr("toasted"), "comment": qsTr("commented on") }
    property var action_icon : { "toast": "image://theme/icon-s-like", "comment": "image://theme/icon-s-message" }

    NotificationsModel {
        id: notifications
        target: "notifications"
        onLoadingChanged: {
            if ( !loading ) {
                if (count === 0) {
                    noData.enabled = true;
                }
            }
        }
    }

    SilicaListView {
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Notifications")
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Reload")
                onClicked: notifications.load()
            }
        }

        ViewPlaceholder {
            id: noData
            enabled: false
            text: qsTr("No notification for you")
        }

        model: notifications

        delegate: BackgroundItem {
            height: notificationItem.height + Theme.paddingMedium
            Row {
                id:notificationItem
                Avatar {
                    id: newsImage
                    size: 128
                    avatarSource: actor.user_avatar

                    Image {
                        anchors {
                            right: parent.right
                            bottom: parent.bottom
                        }
                        width: parent.width * 0.4
                        height: width
                        source: action_icon[notification_type]
                    }
                }
                Column {
                    width: page.width - newsImage.width - Theme.horizontalPageMargin * 2
                    Label {
                        id: message
                        x: Theme.horizontalPageMargin
                        width: parent.width
                        color: Theme.primaryColor
                        wrapMode: Text.WordWrap
                        text: qsTr("%1 %2 %3 your check-in of %5 by %6").arg(actor.first_name).arg(actor.last_name).arg(action_type[notification_type]).arg(beer.beer_name).arg(brewery.brewery_name)
                    }
                    Text {
                        id: createdAt
                        x: Theme.horizontalPageMargin
                        width: parent.width
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.secondaryColor
                        wrapMode: Text.WordWrap
                        text: Qt.formatDateTime(new Date(created_at))
                    }
                }
            }

            Separator {
                id: checkinsSeparator
                anchors {
                    bottom: notificationItem.bottom
                    topMargin: Theme.paddingSmall
                }
                width: parent.width
                color: Theme.primaryColor
                horizontalAlignment: Qt.AlignHCenter
            }

            onClicked: pageStack.push(Qt.resolvedUrl("CheckinDetails.qml"), { "checkin_id": checkin_id })
        }

        BusyIndicator {
            anchors.centerIn: parent
            size: BusyIndicatorSize.Large
            running: notifications.loading
        }
    }

    Component.onCompleted: notifications.load()
}
