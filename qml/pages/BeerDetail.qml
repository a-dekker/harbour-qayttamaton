/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"
import "../components"
import "../utils/UntappdAPI.js" as Untappd

Page {
    id: page

    property string beer_id
    property bool wished: beerInfo.wish_list === "true"

    BeerInfo {
        id: beerInfo
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Activity of this beer")
                onClicked: pageStack.push(Qt.resolvedUrl("BeerActivity.qml"), { "bid": beerInfo.bid, "beer_name": beerInfo.beer_name })
            }
            MenuItem {
                text: qsTr("Add to Wish List")
                visible: !wished
                onClicked: {
                    Untappd.getUserWishlistAdd(function(result) {
                        if ( result === "success" ) {
                            wished = true
                        }
                    }, beer_id)
                }
            }
            MenuItem {
                text: qsTr("Remove from Wish List")
                visible: wished
                onClicked: {
                    Untappd.getUserWishlistDelete(function(result) {
                        if ( result === "success" ) {
                            wished = false
                        }
                    }, beer_id)
                }
            }
            MenuItem {
                text: qsTr("Check In")
                onClicked: {
                    var checkIn = pageStack.push(Qt.resolvedUrl("CheckIn.qml"), {beerInfo: beerInfo})
                    checkIn.parametersChanged.connect(function() {
                        Untappd.postCheckinAdd(function (count, response) {
                            if ( count !== -1 ) {
                                infoBar.show("Check-in Succeeded.")
                            } else {
                                infoBar.show("Check-in Failed.")
                            }
                        }, checkIn.parameters)
                    })
                }
            }
        }

        contentHeight: column.height

        Column {
            id: column

            width: page.width - ( Theme.paddingLarge * 2 )
            x: Theme.paddingLarge
            spacing: Theme.paddingMedium
            PageHeader {
                id: header
                title: beerInfo.beer_name
            }
            Row {
                width: parent.width
                spacing: Theme.paddingLarge
                Image {
                    width: parent.width / 4
                    height: width
                    fillMode: Image.PreserveAspectFit
                    source: beerInfo.beer_label
                }
                Column {
                    width: parent.width * 3 / 4
                    clip: true
                    ScrollingLabel {
                        label_color: Theme.secondaryHighlightColor
                        label_pixelSize: Theme.fontSizeMedium
                        label_text: beerInfo.beer_name
                    }
                    Text {
                        color: Theme.secondaryColor
                        font.pixelSize: Theme.fontSizeSmall
                        text: beerInfo.brewery.brewery_name
                    }
                    Text {
                        color: Theme.secondaryColor
                        font.pixelSize: Theme.fontSizeSmall
                        text: beerInfo.beer_style
                    }
                    Text {
                        color: Theme.secondaryColor
                        font.pixelSize: Theme.fontSizeSmall
                        text: "%1% ABV, %2 IBU".arg(beerInfo.beer_abv).arg(beerInfo.beer_ibu)
                    }
                }
            }

            Row {
                width: parent.width
                height: width * 0.18
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("TOTAL")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: beerInfo.stats.total_count
                        }
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("UNIQUE")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: beerInfo.stats.total_user_count
                        }
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("MONTHLY")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: beerInfo.stats.monthly_count
                        }
                    }
                }
                Rectangle {
                    width: parent.width * 0.25
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("YOU")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: beerInfo.stats.user_count
                        }
                    }
                }
            }

            Row {
                width: parent.width
                height: width * 0.18
                Rectangle {
                    width: parent.width / 3
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: "Rating"
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: beerInfo.rating_count
                        }
                    }
                }
                Rectangle {
                    width: parent.width / 3
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("Global")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: beerInfo.rating_score
                        }
                    }
                }
                Rectangle {
                    width: parent.width / 3
                    height: parent.height
                    color: "#00000000"
                    border.width: 1
                    border.color: Theme.secondaryColor
                    radius: 10
                    Column {
                        anchors.fill: parent
                        Text {
                            width: parent.width
                            height: parent.height * 0.4
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryColor
                            text: qsTr("You Rate")
                        }
                        Text {
                            width: parent.width
                            height: parent.height * 0.6
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.secondaryHighlightColor
                            text: beerInfo.auth_rating
                        }
                    }
                }
            }

            Text {
                width: parent.width
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeMedium
                text: qsTr("Brewery:")
            }
            NameLabel {
                width: parent.width
                enabled: true

                label_image: beerInfo.brewery !== null ? beerInfo.brewery.brewery_label : ""
                label_name: beerInfo.brewery !== null ? beerInfo.brewery.brewery_name : ""
                label_info_1: beerInfo.brewery !== null ? beerInfo.brewery.brewery_type : ""
                label_info_2: beerInfo.brewery !== null ? beerInfo.brewery.country_name : ""

                onClicked: pageStack.push(Qt.resolvedUrl("BreweryDetail.qml"), { "brewery_id": beerInfo.brewery.brewery_id })
            }

            Text {
                width: parent.width
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeMedium
                text: qsTr("Description:")
            }
            Text {
                id: description
                width: parent.width
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WordWrap
                text: beerInfo.beer_description
            }
        }
    }

    BusyIndicator {
        anchors.centerIn: parent
        size: BusyIndicatorSize.Large
        running: beerInfo.loading
    }

    Component.onCompleted: beerInfo.load(beer_id)
}
