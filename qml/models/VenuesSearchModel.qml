/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import "../utils/FoursquareAPI.js" as Foursquare

ThepubLocalModel {
    endpoint: "venues/search"
    parameters: "ll=%1,%2&radius=%3&query=%4".arg(lat).arg(lng).arg(radius).arg(query)
    property string query: ""

    function load() {
        loading = true;
        clear();
        Foursquare.accessAPI( function(result, response, notifications) {
            if ( result ) {
                var items = response.venues;
                var count = Object.keys(response.venues).length;
                for (var n=0; n<count; n++) {
                    append(addAggregatedIconProperty(items[n]));
                }
            } else {
                console.log ("%1: %2".arg(response.code).arg(response.error_detail));
            }
            loading = false;
        }, "GET", endpoint, parameters);
    }

    function addAggregatedIconProperty(item) {
        var category = {
            icon: {
                prefix: "https://foursquare.com/img/categories_v2/building/default_",
                suffix: ".png"
            }
        }

        for (var i = 0; i < item.categories.length; i++) {
            category = item.categories[i]
            if (category.primary)
                break;
        }

        item.aggregatedIcon = "%1%2%3".arg(category.icon.prefix).arg("64").arg(category.icon.suffix);
        return item
    }

    onParametersChanged:  load()
}
