/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0

AbstractInfoModel {
    //
    endpoint: "user/info/"
    target: "user"
    //
    property string id: completed ? info.id : ""
    property string uid: completed ? info.uid : ""
    property string user_name: completed ? info.user_name : ""
    property string first_name: completed ? info.first_name : ""
    property string last_name: completed ? info.last_name : ""
    property string user_cover_photo: completed ? info.user_cover_photo : ""
    property string user_avatar: completed ? info.user_avatar : ""
    property var stats: completed ? info.stats : { "total_checkins": "",
                                                   "total_beers": "",
                                                   "total_friends": "",
                                                   "total_badges": "" }
    property var contact: completed ? info.contact : { "foursquare": "",
                                                       "twitter": "",
                                                       "facebook": "" }
    property string date_joined: completed ? info.date_joined : ""
    property string relationship: completed ? info.relationship : ""

    onRelationshipChanged:  if ( relationship === "self" ) {
                             settings.saveData("foursquare", info.contact.foursquare)
                             settings.saveData("twitter", info.contact.twitter)
                             settings.saveData("facebook", info.contact.facebook)
                         }

}
