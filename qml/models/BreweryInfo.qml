/*
  Copyright (C) 2017 Qayttamaton Project.
  Contact: R Kake <helicalgear@gmail.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import "../utils/UntappdAPI.js" as Untappd

AbstractInfoModel {
    //
    endpoint: "brewery/info/"
    target: "brewery"
    //

    property string brewery_id: completed ? info.brewery_id : ""
    property string brewery_name: completed ? info.brewery_name : ""
    property string brewery_label: completed ? info.brewery_label : ""
    property string brewery_type: completed ? info.brewery_type : ""
    property string country_name: completed ? info.country_name : ""
    property string brewery_description: completed ? info.brewery_description : ""
    property var location: completed ? info.location : { "brewery_address": "",
                                           "brewery_city": "",
                                           "brewery_state": "",
                                           "brewery_lat": "",
                                           "brewery_lng": "" }
    property var brewery: completed ? info.brewery : { "brewery_name": "",
                                                  "brewery_label": "" }
    property var stats: completed ? info.stats : { "total_count": "",
                                         "unique_count": "",
                                         "monthly_count": "",
                                         "user_count": "" }
    property var rating: completed ? info.rating : { "count" : "",
                                            "rating_score" : "" }
    property var contact: completed ? info.contact : { "twitter" : "",
                                          "facebook" : "",
                                          "instagram" : "",
                                          "url" : "" }
    property var beer_list: completed ? info.beer_list : {}
}
